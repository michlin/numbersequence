﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberSequence
{
    /// <summary>
    /// HandleSequences is a class that generates and handles the picking witith the numbers of sequences. 
    /// </summary>
    public class HandleSequences
    {
        private int stepLength = 1;
        private int multipleValue = 0;
        private int apendoCount = 0;
        private string result = "";

        /// <summary>
        /// Property for the Multiple value. The default value is zero.
        /// </summary>
        public int MultipleValue { get { return multipleValue; } set { multipleValue = value; } }
        /// <summary>
        /// Property for the step length value. By default the value is one.
        /// </summary>
        public int StepLength { get { return stepLength; } set { stepLength = value; } }

        /// <summary>
        /// Constructor for HandleSequences that handles the call when no parameter is given (Part A).
        /// </summary>
        public HandleSequences()
        {}

        /// <summary>
        /// Constructor of HandleSequences that handles the call when the call has defined step length and multiple value.
        /// </summary>
        /// <param name="stepLength">A specified step length.</param>
        /// <param name="multipleValue">A multiple value.</param>
        public HandleSequences(int stepLength, int multipleValue)
        {
            StepLength = stepLength;
            MultipleValue = multipleValue;
        }

        /// <summary>
        /// Constructor of HandleSequences that handles the call when the call has step length defined (Part B).
        /// </summary>
        /// <param name="stepLength">A specified step length.</param>
        public HandleSequences(int stepLength)
        {
            this.StepLength = stepLength;
        }

        /// <summary>
        /// Constructor of HandleSequences that handles the call when the call has only Multiple value defined (Part C).
        /// To get another constructor for just the multiple value the conversation from string to int was taken.
        /// </summary>
        /// <param name="multipleValue">A multiple value</param>
        public HandleSequences(string multipleValue)
        {
            if (int.TryParse(multipleValue, out int n))
            {
                MultipleValue = n;
            }
            else
                MultipleValue = 0;

        }

        /// <summary>
        /// Method GenerateSequence generates a sequence of numbers from the specified StartValue to the specified StopValue.
        /// If a step length is specefied the squence will be increased (or decreased) by that value.
        /// The choice of List of string was done to easy return the value of "Apendo" to the output.
        /// If the multiple value is not zero and i is a multiple of that value then "Apendo" is added to the result. 
        /// </summary>
        /// <param name="Startvalue">Start value of the sequence</param>
        /// <param name="StopValue">Stop value of the sequene</param>
        /// <returns>A sequence of numbers and strings as a string list.</returns>
        public List<string> GenerateSequence(int Startvalue, int StopValue)
        {
            int i = 0;
            List<string> numSequence = new List<string>();
            if (Startvalue > StopValue)
            {
                for (i = Startvalue; i >= StopValue; i -= stepLength)
                {
                    numSequence.Add(i.ToString());
                }
            }
            else
            {
                for (i = Startvalue; i <= StopValue; i += stepLength)
                {
                    if ((MultipleValue != 0) && (i % multipleValue == 0))
                    {
                        numSequence.Add("Apendo");
                    }
                    else
                        numSequence.Add(i.ToString());
                }
            }
            return numSequence;
        }

        /// <summary>
        /// Method PickNumberInSecquence picks the number on the specefied index.
        /// If a step length is specefied the sequence will be increased by that value.
        /// It is decreased by one to match the index of records within the generated sequence.
        /// </summary>
        /// <param name="Startvalue">Start value of the sequence.</param>
        /// <param name="StopValue">Stop value of the sequencce.</param>
        /// <param name="index">Specefied index where the result value is picked from.</param>
        /// <returns></returns>
        public string PickNumberInSecquence(int Startvalue, int StopValue, int index)
        {
            int i = 0;
            List<string> numSequence = new List<string>();
            result = "";
            index = index - 1; // Because the count start at zero.
            if (Startvalue < StopValue)
            {
                for (i = Startvalue; i <= StopValue; i += StepLength)
                {
                    if ((MultipleValue != 0) && (i % multipleValue == 0))
                    {
                        numSequence.Add("Apendo");
                        apendoCount += 1;

                    }
                    else
                        numSequence.Add(i.ToString());
                }
            }
            if (index < numSequence.Count)
                result = numSequence[index];
            else
                result = "index out of bounds, please choose a value in the range of the sequence.";

            return result;
        }
        /// <summary>
        /// Method getApendoCount Returns the count of how many times "Apendo" has occoured in the previous output.
        /// </summary>
        /// <returns>Returns the count of how many times "Apendo" as a string</returns>
        public string GetApendoCount()
        {
            return apendoCount.ToString();
        }
    }
}
