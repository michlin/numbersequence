﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NumberSequence
{
    /// <summary>
    /// OutputWindow handles the output to the window form. 
    /// </summary>
    public partial class OutputWindow : Form
    {
        public OutputWindow()
        {
            InitializeComponent();
        }

        HandleSequences sequenceHandler;
        /// <summary>
        /// Method printPartA prints the Part A of the test.
        /// </summary>
        private void PrintPartA()
        {
            sequenceHandler = new HandleSequences();
            List<string> NumberSequence = sequenceHandler.GenerateSequence(3, 5);
            List<string> NumberSequenceReverse = sequenceHandler.GenerateSequence(6, 3);
            OutputTextbox.Text += "Number sequence 3 to 5" + Environment.NewLine;
            OutputTextbox.Text += String.Join(Environment.NewLine, NumberSequence);
            OutputTextbox.Text += Environment.NewLine;
            OutputTextbox.Text += "Reverse number sequence 6 to 3" + Environment.NewLine;
            OutputTextbox.Text += String.Join(Environment.NewLine, NumberSequenceReverse);
            OutputTextbox.Text += Environment.NewLine;
        }

        /// <summary>
        /// Method printPartB prints the part B of the test.
        /// </summary>
        private void PrintPartB()
        {
            sequenceHandler = new HandleSequences(3, 4);
            List<string> NumberSequenceStepLength = sequenceHandler.GenerateSequence(5, 15);
            OutputTextbox.Text += "Numbers with Steplength " + sequenceHandler.StepLength + " : " + Environment.NewLine;
            OutputTextbox.Text += String.Join(Environment.NewLine, NumberSequenceStepLength);
            OutputTextbox.Text += Environment.NewLine;
        }

        /// <summary>
        /// Method printPartC prints the part C of the test.
        /// </summary>
        private void PrintPartC()
        {

            string multipleValue = "4";
            sequenceHandler = new HandleSequences(multipleValue);
            string PickedNumber = sequenceHandler.PickNumberInSecquence(12, 16, 3);
            OutputTextbox.Text += "In a sequence from 12 to 16 the third number is " + PickedNumber + Environment.NewLine;
            OutputTextbox.Text += "With a multiple value of : " + multipleValue + " to the number sequence the appearence of Apendo is : " + sequenceHandler.GetApendoCount() + Environment.NewLine;
            OutputTextbox.Text += Environment.NewLine;
        }
        /// <summary>
        /// SolveAllPartsButton_Click handles the event when the button "Solve all parts" is clicked.
        /// </summary>
        /// <param name="sender">A sender object.</param>
        /// <param name="e">An eventargs.</param>
        private void SolveAllPartsButton_Click(object sender, EventArgs e)
        {
            OutputTextbox.Text = "";
            PrintPartA();
            PrintPartB();
            PrintPartC();

        }

        /// <summary>
        /// Method SolvePartAButon_Click handles the event when the button "Solve Part A" is clicked.
        /// </summary>
        /// <param name="sender">A sender object.</param>
        /// <param name="e">An eventargs.</param>
        private void SolvePartAButon_Click(object sender, EventArgs e)
        {
            OutputTextbox.Text = "";
            PrintPartA();
        }

        /// <summary>
        /// Method SolvePartBButton_Click handles the event when the button "Solve Part B" is clicked.
        /// </summary>
        /// <param name="sender">A sender object.</param>
        /// <param name="e">An eventargs.</param>
        private void SolvePartBButton_Click(object sender, EventArgs e)
        {
            OutputTextbox.Text = "";
            PrintPartB();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">A sender object.</param>
        /// <param name="e">An eventargs.</param>
        private void SolvePartCButton_Click(object sender, EventArgs e)
        {
            OutputTextbox.Text = "";
            PrintPartC();
        }
    }
}
