﻿namespace NumberSequence
{
    partial class OutputWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SolveAllParts = new System.Windows.Forms.Button();
            this.OutputTextbox = new System.Windows.Forms.RichTextBox();
            this.OutputLabel = new System.Windows.Forms.Label();
            this.SolvePartAButton = new System.Windows.Forms.Button();
            this.SolvePartBButton = new System.Windows.Forms.Button();
            this.SolvePartCButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SolveAllParts
            // 
            this.SolveAllParts.Location = new System.Drawing.Point(13, 13);
            this.SolveAllParts.Name = "SolveAllParts";
            this.SolveAllParts.Size = new System.Drawing.Size(122, 23);
            this.SolveAllParts.TabIndex = 0;
            this.SolveAllParts.Text = "Solve all parts";
            this.SolveAllParts.UseVisualStyleBackColor = true;
            this.SolveAllParts.Click += new System.EventHandler(this.SolveAllPartsButton_Click);
            // 
            // OutputTextbox
            // 
            this.OutputTextbox.Location = new System.Drawing.Point(13, 61);
            this.OutputTextbox.Name = "OutputTextbox";
            this.OutputTextbox.Size = new System.Drawing.Size(491, 534);
            this.OutputTextbox.TabIndex = 1;
            this.OutputTextbox.Text = "";
            // 
            // OutputLabel
            // 
            this.OutputLabel.AutoSize = true;
            this.OutputLabel.Location = new System.Drawing.Point(13, 42);
            this.OutputLabel.Name = "OutputLabel";
            this.OutputLabel.Size = new System.Drawing.Size(39, 13);
            this.OutputLabel.TabIndex = 2;
            this.OutputLabel.Text = "Output";
            // 
            // SolvePartAButton
            // 
            this.SolvePartAButton.Location = new System.Drawing.Point(142, 12);
            this.SolvePartAButton.Name = "SolvePartAButton";
            this.SolvePartAButton.Size = new System.Drawing.Size(75, 23);
            this.SolvePartAButton.TabIndex = 3;
            this.SolvePartAButton.Text = "Solve Part A";
            this.SolvePartAButton.UseVisualStyleBackColor = true;
            this.SolvePartAButton.Click += new System.EventHandler(this.SolvePartAButon_Click);
            // 
            // SolvePartBButton
            // 
            this.SolvePartBButton.Location = new System.Drawing.Point(224, 12);
            this.SolvePartBButton.Name = "SolvePartBButton";
            this.SolvePartBButton.Size = new System.Drawing.Size(75, 23);
            this.SolvePartBButton.TabIndex = 4;
            this.SolvePartBButton.Text = "Solve Part B";
            this.SolvePartBButton.UseVisualStyleBackColor = true;
            this.SolvePartBButton.Click += new System.EventHandler(this.SolvePartBButton_Click);
            // 
            // SolvePartCButton
            // 
            this.SolvePartCButton.Location = new System.Drawing.Point(306, 11);
            this.SolvePartCButton.Name = "SolvePartCButton";
            this.SolvePartCButton.Size = new System.Drawing.Size(75, 23);
            this.SolvePartCButton.TabIndex = 5;
            this.SolvePartCButton.Text = "Solve Part C";
            this.SolvePartCButton.UseVisualStyleBackColor = true;
            this.SolvePartCButton.Click += new System.EventHandler(this.SolvePartCButton_Click);
            // 
            // OutputWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 607);
            this.Controls.Add(this.SolvePartCButton);
            this.Controls.Add(this.SolvePartBButton);
            this.Controls.Add(this.SolvePartAButton);
            this.Controls.Add(this.OutputLabel);
            this.Controls.Add(this.OutputTextbox);
            this.Controls.Add(this.SolveAllParts);
            this.Name = "OutputWindow";
            this.Text = "Sequence Of Numbers";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SolveAllParts;
        private System.Windows.Forms.RichTextBox OutputTextbox;
        private System.Windows.Forms.Label OutputLabel;
        private System.Windows.Forms.Button SolvePartAButton;
        private System.Windows.Forms.Button SolvePartBButton;
        private System.Windows.Forms.Button SolvePartCButton;
    }
}

